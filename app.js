const express = require("express");
const bodyParser = require("body-parser");

const shutdownRoute = require("./routes/pcremote/pcremote");
const volumeRoute = require("./routes/volume/volume");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(require("cors")());

app.use("/api/pcremote", shutdownRoute);
app.use("/api/volume", volumeRoute);

module.exports = app;
