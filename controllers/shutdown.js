const exec = require("child_process").exec;
const moment = require("moment");

let remainingTime = null;

module.exports.shutdown = async (req, res) => {
  console.log("Shutdown NOW!");
  res.status(200).json({
    message: "Shutdown now!",
  });
};

module.exports.cancel = async (req, res) => {
  function shutdown(callback) {
    exec("shutdown -a", function (error, stdout, stderr) {
      callback(stdout);
    });
    console.log("Cancel shutdown!");
    console.log(moment().format("MMMM Do YYYY, h:mm:ss a"));
  }

  try {
    shutdown(function (output) {
      console.log(output);
    });
    remainingTime = null;

    res.status(200).json({
      message: "Shutdown was canceled!",
    });
  } catch (e) {
    res.status(404).json({
      message: `Error : ${e}`,
    });
  }
};

module.exports.timer = async (req, res) => {
  console.log("timer", req.body);
  const { timer } = req.body;
  // .format("MMMM DD YYYY, h:mm:ss a")
  remainingTime = moment().add(timer, "minutes");
  if (timer) {
    try {
      exec(`shutdown -s -t ${timer * 60}`);
      res.status(201).json({
        message: `Timer was created on : ${timer * 60}`,
        remainingTime,
        startedOf: moment().format("MMMM DD YYYY, h:mm:ss a"),
      });
    } catch (e) {
      console.log("Error");
      res.status(400).json({
        message: "Something went wrong",
      });
      return;
    }
  }
};

module.exports.remainingTime = async (req, res) => {
  if (remainingTime) {
    res.status(200).json({
      message: moment(remainingTime) - moment(),
    });
  } else {
    res.status(404).json({
      message: 'Timer is not exist!'
    })
  }
};
