import React, { useState, useEffect } from "react";
import axios from "axios";
import { ToastsContainer, ToastsStore } from "react-toasts";
import Timer from "../timer";
import "./index.scss";

export const Form = () => {
  const [form, setForm] = useState({
    timer: "",
  });
  const [remainingTime, setRemainingTime] = useState(null);

  const onCancel = (e) => {
    e.preventDefault();
    axios.get(`http://192.168.31.89:54321/api/pcremote/cancel`).then((res) => {
      ToastsStore.warning(`Timer was removed`);
      setRemainingTime(null);
    });
    setTime(null);
  };

  const setTimer = (e) => {
    e.preventDefault();
    if (form.timer) {
      axios
        .post(`http://192.168.31.89:54321/api/pcremote/timer`, form)
        .then((res) => {
          if (res.status === 201) {
            ToastsStore.success(`Timer has been created on ${form.timer}min`);
            setForm({ timer: "" });
            axios
              .get("http://192.168.31.89:54321/api/pcremote/remainingTime")
              .then((res) => {
                setTime(res.data.message);
              });
          } else {
            ToastsStore.warning(`${res.data.message}`);
          }
        });
    }
  };

  const [time, setTime] = useState("");

  useEffect(() => {
    axios
      .get("http://192.168.31.89:54321/api/pcremote/remainingTime")
      .then((res) => {
        setTime(res.data.message);
      });
  }, []);

  const onChange = (e) => {
    return setForm({
      timer: e.target.value,
    });
  };
  return (
    <div className="form-block">
      <form onSubmit={setTimer} className="form">
        <input
          placeholder="Set timer in minutes"
          name="timer"
          value={form.timer}
          onChange={onChange}
          type="text"
          pattern="[0-9]*"
          className="input"
        />
        <button type="submit" className="button">
          Set timer
        </button>
      </form>
      <button onClick={onCancel} className="button danger">
        Cancel shutdown
      </button>
      <ToastsContainer store={ToastsStore} />
      <Timer time={time} />
    </div>
  );
};

export default Form;
