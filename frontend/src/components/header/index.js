import React from "react";
import "./index.scss";

export const Header = () => (
  <header className="header">
    <div className="logo">My Home</div>
  </header>
);

export default Header;
