import React from 'react';
import Header from '../header';
import Form from '../form';
import "./index.scss";

export const Main = () => {
  console.log('main page');
  return (
    <div className="main">
      <Header />
      <Form />
    </div>
  )
}

export default Main;