import React from "react";
import "./index.scss";

export const Timer = ({ time }) => {
  return (
    <>
      {time && (
        <span className="timer">
          До выключения: {Math.round(time / 60000)} минут
        </span>
      )}
    </>
  );
};

export default Timer;
