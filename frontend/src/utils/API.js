import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:54321/",
  responseType: "json"
});