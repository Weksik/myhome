const express = require("express");
const controller = require("../../controllers/volume");
const router = express.Router();

router.get("/", controller.volume);

module.exports = router;
